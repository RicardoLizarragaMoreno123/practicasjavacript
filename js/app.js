// obtener el objeto button de calcular 
const btnCalcular = document.getElementById("btnCalcular");
btnCalcular.addEventListener('click', function(){

    let valorAuto = document.getElementById('txtvalorAuto').value;
    let porcentaje = document.getElementById('txtPorcentaje').value;
    let plazo = document.getElementById('plazos').value;
    
    // Hacer Los Calculos
    let pagoInicial = valorAuto * (porcentaje/100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin/plazo;

    //Mostrar Los Datos

    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos;



});
// codificación el botón de limpiar
const btnLimpiar = document.getElementById("btnLimpiar");
btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtvalorAuto').value = "";
    document.getElementById('txtPorcentaje').value = "";
    document.getElementById('txtPagoInicial').value = "";
    document.getElementById('txtTotalFin').value = "";
    document.getElementById('txtPagoMensual').value = "";
});
