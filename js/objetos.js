const arreglo = [7, 10, 2, 15, 8, 6, 9, 5, 12, 14];

        function mostrarElementos() {
            document.getElementById("result").textContent = "Elementos del arreglo: " + arreglo.join(", ");
        }

        function mostrarNumerosPares() {
            const pares = arreglo.filter(numero => numero % 2 === 0);
            document.getElementById("result").textContent = "Números Pares: " + pares.join(", ");
        }

        function mostrarValorMayorYPosicion() {
            const max = Math.max(...arreglo);
            const posicion = arreglo.indexOf(max);
            document.getElementById("result").textContent = "Valor Mayor: " + max + " (Posición: " + posicion + ")";
        }

        function mostrarPromedio() {
            const promedio = arreglo.reduce((a, b) => a + b, 0) / arreglo.length;
            document.getElementById("result").textContent = "Promedio: " + promedio;
        }

        function mostrarValorMenorYPosicion() {
            const min = Math.min(...arreglo);
            const posicion = arreglo.indexOf(min);
            document.getElementById("result").textContent = "Valor Menor: " + min + " (Posición: " + posicion + ")";
        }

        function mostrarPorcentajeSimetria() {
            const mitad1 = arreglo.slice(0, arreglo.length / 2);
            const mitad2 = arreglo.slice(arreglo.length / 2);
            const simetrico = mitad1.join("") === mitad2.reverse().join("");
            const porcentaje = (mitad1.length / arreglo.length) * 100;
            
            document.getElementById("result").textContent = "Porcentaje de Simetría: " + porcentaje + "% (Es simétrico: " + simetrico + ")";
        }