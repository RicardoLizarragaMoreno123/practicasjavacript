function calcularIMC() {
    var peso = parseFloat(document.getElementById("peso").value);
    var altura = parseFloat(document.getElementById("altura").value);
    
    if (!isNaN(peso) && !isNaN(altura) && altura > 0) { // Asegúrate de que la altura sea mayor que 0
        var imc = peso / (altura * altura);
        document.getElementById("resultadoIMC").textContent = imc.toFixed(2); // Redondea a 2 decimales
    } else {
        document.getElementById("resultadoIMC").textContent = "Ingrese valores válidos.";
    }
}
