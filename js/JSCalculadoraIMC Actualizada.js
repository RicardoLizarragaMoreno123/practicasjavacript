function calcularIMC() {
    var edad = parseFloat(document.getElementById("edad").value);
    var sexo = document.getElementById("sexo").value;
    var peso = parseFloat(document.getElementById("peso").value);
    var altura = parseFloat(document.getElementById("altura").value);

    var imc = peso / (altura * altura);

    var resultadoIMC = document.getElementById("resultadoIMC");
    resultadoIMC.textContent = "Tu IMC es: " + imc.toFixed(2);

    if (sexo === "masculino") {
        if (edad >= 18) {
            if (imc < 17) {
                resultadoIMC.textContent += " (Infrapeso)";
            } else if (imc < 18.5) {
                resultadoIMC.textContent += " (Bajo peso)";
            } else if (imc < 24.9) {
                resultadoIMC.textContent += " (Peso saludable)";
            } else if (imc < 29.9) {
                resultadoIMC.textContent += " (Sobrepeso)";
            } else if (imc < 34.9) {
                resultadoIMC.textContent += " (Obesidad clase 1)";
            } else if (imc < 39.9) {
                resultadoIMC.textContent += " (Obesidad clase 2)";
            } else {
                resultadoIMC.textContent += " (Obesidad clase 3)";
            }
        } else {
            resultadoIMC.textContent += " (IMC no válido para menores de 18 años)";
        }
    } else {
        if (edad >= 18) {
            if (imc < 17) {
                resultadoIMC.textContent += " (Infrapeso)";
            } else if (imc < 18.5) {
                resultadoIMC.textContent += " (Bajo peso)";
            } else if (imc < 24.9) {
                resultadoIMC.textContent += " (Peso saludable)";
            } else if (imc < 29.9) {
                resultadoIMC.textContent += " (Sobrepeso)";
            } else {
                resultadoIMC.textContent += " (Obesidad)";
            }
        } else {
            resultadoIMC.textContent += " (IMC no válido para menores de 18 años)";
        }
    }
}