function generateTable() {
    const selectedNumber = parseInt(document.getElementById("selectNumber").value);
    const tableImage = document.getElementById("tableImage");

    const table = document.createElement("table");
    table.style.border = "1px solid black";

    for (let i = 1; i <= 10; i++) {
        const row = table.insertRow();
        const cell1 = row.insertCell(0);
        const cell2 = row.insertCell(1);

        cell1.innerHTML = `${selectedNumber} x ${i}`;
        cell2.innerHTML = selectedNumber * i;
    }

    const img = new Image();
    img.src = "data:image/svg+xml;base64," + btoa("<svg xmlns='http://www.w3.org/2000/svg' width='300' height='200' version='1.1'><foreignObject width='100%' height='100%'><div xmlns='http://www.w3.org/1999/xhtml'>" + table.outerHTML + "</div></foreignObject></svg>");
    tableImage.innerHTML = "";
    tableImage.appendChild(img);
}